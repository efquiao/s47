import ReactReact, { useState, useEffect } from 'react';

import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

function Register() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const passwordConfirm = 'password'
 // const [isDisabled, setIsDisabled] = useState(true);

  // useEffect(()=>{
  //   console.log(email)
  // },[email])
  // useEffect(()=>{
  //   console.log(password)
    
  // },[password])
  // useEffect(()=>{
  //   console.log(passwordConfirm)
  // },[passwordConfirm])

  useEffect(()=>{
   let isEmailIsNotEmpty = email !== '';
   let isPasswordNotEmpty = password !== '';
   let isPasswordMatched = password === passwordConfirm;


  },[email,password,passwordConfirm])

  const register = (e) =>{
    e.preventDefault()
    if(password == passwordConfirm){
      alert('You are now logged in')
    }else{
      alert('Wrong password! Please try again.')
    }
    
  }

  return (
    <Container fluid>
      <h3>Login</h3>
      <Form onSubmit = {register}>
        <Form.Group>
          <Form.Label>Email address</Form.Label>
          <Form.Control type='email' placeholder='Enter email' required value ={email}onChange = {(e)=>setEmail(e.target.value)}/>

        </Form.Group>
        <Form.Group>
        <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" required value ={password} onChange = {(e)=>setPassword(e.target.value)}/>
        </Form.Group>
        
        <Button variant="primary" type="submit" >Login</Button>
      </Form>
    </Container>
  );
}

export default Register;
