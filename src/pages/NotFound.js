import React from 'react'

const NotFound = () => {
  return (
    <div>
      <h3>Page Not Found</h3>
      <p>Go back to the <a href='/'>homepage</a>.</p>
    </div>
  )
}

export default NotFound