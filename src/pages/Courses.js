import React from 'react'
import Course from '../components/Course'
import Container from 'react-bootstrap/Container'
import courses from '../mock-data/courses'

const Courses = () => {
  const CourseCards = courses.map((course)=>{
  
    console.log(course)
    return(
      <Course 
        key={course.id} 
        name={course.name} 
        desc={course.description}
        price={course.price}
        onOffer={course.onOffer}
        // course={course}
     
      />
    )
  })

  
  return (
    <Container fluid>
      <h1>Courses</h1>
      {CourseCards}
    </Container>
  )
}

export default Courses