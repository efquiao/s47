import React,{useState, useEffect} from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const Course = ({ name, desc, price, onOffer }) => {

    const[isDisabled, setIsDisabled] = useState(false)
    const[seats, setSeats] = useState(10)

    useEffect(()=>{
      if(seats===0){
        setIsDisabled(true)
      }
    },[seats])
   

  return (
    <Row gap={4}>
      <Col xs={12}>
        <Card className='card-highlight'>
          <Card.Body>
            <Card.Title>
              <h4>{name}</h4>
            </Card.Title>
            <Card.Text>{desc}</Card.Text>
            <h6>price</h6>
            <p>PhP {price}</p>
            <h6>Seats</h6>
            <p>{seats}</p>
            
            <Button variant='primary' onClick = {()=>setSeats(seats-1)} disabled={isDisabled}>Enroll</Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
};

export default Course;
