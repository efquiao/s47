import React,{useState,useEffect} from 'react';
import Container from 'react-bootstrap/Container'


function Counter() {
  const[count, setCount] = useState(0)

  useEffect(()=>{
    document.title = `You clicked ${count} times`
    console.log('render')
  },[count])
  const add =()=>{
    setCount(prev=>prev+1)
  }
  return (
    <Container fluid>
      <p>You Clicked {count} times</p>
      <button onClick={add}>Click Me</button>
    </Container>
  )
}

export default Counter;
