import React from 'react'

function Welcome(props) {
  return (
    <h1>Hello {props.name} and {props.age}</h1>
  )
}

export default Welcome