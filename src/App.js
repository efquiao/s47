import React from 'react'
import AppNavBar from './components/AppNavBar';

import Home from './pages/Home'
import Courses from './pages/Courses';
import Highlights from './components/Highlights';
import Counter from './components/Counter';
import Register from './pages/Register';
import Login from './pages/Login';
import NotFound from './pages/NotFound'

//Base Imports
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

//CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
const App = () => {
  return (
    <BrowserRouter>
      <AppNavBar />
      <Routes>
        <Route path ='/' element ={<Home/>}/>
        <Route path ='/courses' element = {<Courses/>}/>
        <Route path ='/register' element = {<Register/>}/>
        <Route path ='/login' element = {<Login/>}/>
        <Route path ='*' element = {<NotFound/>} />
      
      </Routes>

    </BrowserRouter>
  )
}

export default App